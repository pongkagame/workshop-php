<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Workshop PHP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Workshop PHP</h1>
    <?php
        // echo "Fundamental, PHP";

        // $hello="Fundamental, PHP";

        // echo $hello;
        // $integer = 10;
        // $string = "Hello";
        // $boolean = true;
        // $float = 34.7;
        // $null = null;

        // $hello = "PHP Fundamental";

        // echo strlen($hello); //jumlah karakter
        // echo "</br>";
        // echo str_word_count($hello); //jumlah kata
        // echo "</br>";
        // echo strrev($hello); //membalik urutan kata
        // echo "</br>";
        // echo strpos($hello, "Fun"); //mencari pos pencarian
        // echo "</br>";
        // $replace=str_replace("PHP", "Laravel", $hello);
        // echo $replace;

        // define("greeting", "Hello Guys !");
        // echo greeting;

        // $angka = 7577;
        // // $angka += 
        // $angka = $angka * 765;

        // echo $angka;

        // $nilai = 54;
        // $abstain = 5;

        // if($nilai>60 && $abstain<=3){
        //     $status = "Lulus";
        // }else{
        //     $status = "Tidak Lulus";
        // }
        // echo $status;

        // $skor = 5;
        // switch($skor){
        //     case 5:
        //     $rating="Baik Sekali";
        //     break;
        //     case 4:
        //     $rating="Baik";
        //     break;
        //     case 3:
        //     $rating="Cukup";
        //     break;
        //     case 2:
        //     $rating="Kurang";
        //     default:
        //     $rating="Tidak Valid";

        // }
        // echo $rating;

        // for($i=1;$i<=100;$i++){
        //     echo $i."<br>";
        // }
        // $i = 1000;
        // while($i>0){
        //     echo $i."<br/>";
        //     $i--;
        // }

        // $angkapertama=0;
        // $angkakedua=1;
 
        // for ($i=0; $i<10; $i++)
        //     {
        //         $hasil = $angkapertama + $angkakedua;
        //         $angkapertama = $angkakedua;
        //         $angkakedua = $hasil;
        //         echo $hasil."<br>";
        // }

        // fuction Ctof($tempC){
        //     $temF = ($tempC * 9/5) + 32;
        //     return $temF;
        // }

        // $celcion = 38;

        // function KonversiC($suhuC){
        //     $f = ($suhuC * 9/5) + 32;
        //     $r = ($suhuC * 4/5);
        //     $k = ($suhuC * 273.15);
        // }
        // $days = array("Monday","Tuesday","Wednesday");
        // $days = array(
        //     "Senin" => "Monday",
        //     "Selasa" => "Tuesday",
        //     "Rabu" => "Wednesday");
        // var_dump($days);
        
        //$days = array("Monday","Tuesday","Wednesday");

        // echo $days[1];

        // $days = array(
        //          "Senin" => "Monday",
        //          "Selasa" => "Tuesday",
        //          "Rabu" => "Wednesday",
        //          "Kamis" => "Thurday",
        //          "Jumat" => "Friday",
        //          "Sabtu" => "Saturday",
        //          "Minggu" => "Sunday");

        // echo $days["selasa"];

        // foreach ($days as $key => $day){
            // echo $day;
            // echo key."-".$day."br/>";
            // $days["Kamis"]="Thursday";

            // $days["Jumat"]="Friday";

            // array_push($days, "Saturday");
            // array_push($days, "Sunday");

            // var_dump($days);
        // }
        // unset($days["Jumat"]);

        //$search = array_search("Tuesday",$days);

        // var_dump($search);
        // var_dump($days);
        //echo $search;

        class Animal{
            private $food="Herbivore";
            //public $food;
            public $feet;

            public function __constract($food, $feet){
                $this->food = $food;
                $this->feet = $feet;
            }
            public function sounds(){
                return "Meowww";
            }
            // //Untuk mengakses private getter
            // public function getFood(){//getter
            //     return $this->food;
            // }
            // public function setFood($newFood){//setter
            //     $this->food=$newFood;
            // }

        }
        //inheritance (turunan)
        class Mammal extends Animal{
            public $hair="white";
        }
        class Reptile extends Animal{
            public $scales = "green";
        }



        // $cat = new Animal(4);
        // // $cat = new Animal("Carnivore",4);
        // // $cat->feet=6;
        // // echo $cat->sounds();
        // // echo "</br>Kakinya : ".$cat->feet;
        // //echo $cat->food;
        // // var_dump($cat);
        // $cat->setFood('Omnivora');
        // echo $cat->getFood();
    ?>
</div>
</body>
</html>